﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HippoProject.Web.ViewModels
{
    public class AttributeViewModel
    {
        public AttributeViewModel()
        {
        }

        public int AttributeId { get; set; }

        
        [Required]
        public string AttributeName { get; set; }
    }
}