﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace HippoProject.Web.ViewModels
{
    public class PersonViewModel
    {
        private IEnumerable<PersonAttributeViewModel> personAttributes;

        public PersonViewModel()
        {
         
        }

        [ScaffoldColumn(false)]
        public int PersonId { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string Surname { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DOB { get; set; }

        [ReadOnly(true)]
        public string Age { get; set; }

        //[UIHint("PersonAttribute")]
        public IEnumerable<PersonAttributeViewModel> PersonAttributes
        {
            get { return personAttributes ?? (personAttributes = new List<PersonAttributeViewModel>()); }
            set { personAttributes = value; }
        }
    }
}