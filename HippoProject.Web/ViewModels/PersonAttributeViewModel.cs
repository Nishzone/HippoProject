﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HippoProject.Web.ViewModels
{
    public class PersonAttributeViewModel
    {
        public int PersonAttributeId  { get; set; }

        //public int AttributeId { get; set; }
        //public string Index { get; set; }

        //public PersonViewModel Person { get; set; }

        [Display(Name = "Name")]
        [UIHint("Attribute")]
        public  AttributeViewModel Attribute { get; set; }

        //public string AttributeName { get; set; }

        [Display(Name = "Description")]
        public string AttributeDescription { get; set; }

    }
}