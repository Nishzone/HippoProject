﻿using HippoProject.Service;
using HippoProject.Web.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HippoProject.Web.Controllers
{
    public class AttributeController : Controller
    {
        private IAttributeService attributeService;

        public AttributeController(IAttributeService attributeService)
        {
            this.attributeService = attributeService;
        }

        // GET: Attribute
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAttributes([DataSourceRequest]DataSourceRequest request)
        {
            var attributes = attributeService.GetAttributes().ToList();
            List<AttributeViewModel> attributeList = new List<AttributeViewModel>();

            foreach(var attribute in attributes)
            {
                var attributeViewModel = new AttributeViewModel();
                attributeViewModel.AttributeId = attribute.AttributeId;
                attributeViewModel.AttributeName = attribute.AttributeName;
                attributeList.Add(attributeViewModel);
            }

            DataSourceResult result = attributeList.ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateAttribute([DataSourceRequest] DataSourceRequest request, AttributeViewModel attributeViewModel)
        {
            if (attributeViewModel != null && ModelState.IsValid)
            {
                var attribute = new Core.Model.Attribute();
                attribute.AttributeName = attributeViewModel.AttributeName;
                attributeService.InsertAttribute(attribute);
            }

            return Json(new[] { attributeViewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdateAttribute([DataSourceRequest] DataSourceRequest request, AttributeViewModel attributeViewModel)
        {
            if (attributeViewModel != null && ModelState.IsValid)
            {
                var attribute = attributeService.GetAttributeById(attributeViewModel.AttributeId);
                attribute.AttributeName = attributeViewModel.AttributeName;
                attributeService.UpdateAttribute(attribute);
            }

            return Json(new[] { attributeViewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteAttribute([DataSourceRequest] DataSourceRequest request, AttributeViewModel attributeViewModel)
        {
            if (attributeViewModel != null)
            {
                var attribute = attributeService.GetAttributeById(attributeViewModel.AttributeId);
                attributeService.DeleteAttribute(attribute);
            }

            return Json(new[] { attributeViewModel }.ToDataSourceResult(request, ModelState));
        }

    }
}