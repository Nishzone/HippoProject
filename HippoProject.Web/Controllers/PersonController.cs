﻿using HippoProject.Core.Model;
using HippoProject.Service;
using HippoProject.Web.ViewModels;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HippoProject.Web.Controllers
{
    public class PersonController : Controller
    {
        private IPersonService personService;
        private IAttributeService attributeService;

        public PersonController(IPersonService personService, IAttributeService attributeService)
        {
            this.personService = personService;
            this.attributeService = attributeService;
        }

        public ActionResult Index()
        {
            PopulateAttributes();
            return View();
        }

        public ActionResult GetPeople([DataSourceRequest]DataSourceRequest request)
        {
            var people = personService.GetPeople().ToList();
           
            var personViewModelList = new List<PersonViewModel>();

            foreach (var person in people)
            {
                var model = new PersonViewModel();
                model.PersonId = person.PersonId;
                model.FirstName = person.FirstName;
                model.Surname = person.Surname;
                model.DOB = person.DOB;
                model.Age = GetAge(model.DOB);
               var personAttributeViewModelList = new List<PersonAttributeViewModel>();
                foreach (var personAttribute in person.PersonAttributes)
                {
                    var personAttributeViewModel = new PersonAttributeViewModel();
                    personAttributeViewModel.PersonAttributeId = personAttribute.PersonAttributeId;
                    personAttributeViewModel.Attribute = new AttributeViewModel();
                    personAttributeViewModel.Attribute.AttributeId = personAttribute.AttributeId;
                    personAttributeViewModel.Attribute.AttributeName = personAttribute.Attribute.AttributeName;
                    personAttributeViewModel.AttributeDescription = personAttribute.AttributeDescription;
                    personAttributeViewModelList.Add(personAttributeViewModel);
                }
                model.PersonAttributes = personAttributeViewModelList;
                personViewModelList.Add(model);
            }

            DataSourceResult result = personViewModelList.ToDataSourceResult(request);
            return Json(result);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreatePerson([DataSourceRequest] DataSourceRequest request, PersonViewModel personViewModel)
        {
            if (personViewModel != null && ModelState.IsValid)
            {
                personViewModel.Age = GetAge(personViewModel.DOB);
                var person = new Core.Model.Person();
                SetModel(personViewModel, ref person);
                personService.InsertPerson(person);
            }

            return Json(new[] { personViewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePerson([DataSourceRequest] DataSourceRequest request, PersonViewModel personViewModel)
        {
            if (personViewModel != null && ModelState.IsValid)
            {
                var person = personService.GetPersonById(personViewModel.PersonId);
                personViewModel.Age = GetAge(personViewModel.DOB);
                SetModel(personViewModel,ref person);
                var activePersonAttributes = personViewModel.PersonAttributes.Select(p => p.PersonAttributeId);
                personService.UpdatePerson(person, activePersonAttributes);
            }
            return Json(new[] { personViewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreatePersonAttribute([DataSourceRequest] DataSourceRequest request, PersonAttributeViewModel personAttributeViewModel)
        {
            if (personAttributeViewModel != null && ModelState.IsValid)
            {
               
            }

            return Json(new[] { personAttributeViewModel }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpdatePersonAttribute([DataSourceRequest] DataSourceRequest request, PersonAttributeViewModel personAttributeViewModel)
        {
            if (personAttributeViewModel != null && ModelState.IsValid)
            {
                
            }
            return Json(new[] { personAttributeViewModel }.ToDataSourceResult(request, ModelState));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletePersonAttribute([DataSourceRequest] DataSourceRequest request, PersonAttributeViewModel personAttributeViewModel)
        {
            if (personAttributeViewModel != null && ModelState.IsValid)
            {

            }
            return Json(new[] { personAttributeViewModel }.ToDataSourceResult(request, ModelState));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeletePerson([DataSourceRequest] DataSourceRequest request, PersonViewModel personViewModel)
        {
            if (personViewModel != null)
            {
                var person = personService.GetPersonById(personViewModel.PersonId);

                personService.DeletePerson(person);
            }

            return Json(new[] { personViewModel }.ToDataSourceResult(request, ModelState));
        }

        private string GetAge(DateTime? dob)
        {
            string age = string.Empty;
            if (dob.HasValue) {
                int calculatedAge = DateTime.Now.Year - dob.Value.Year;
                if (dob > DateTime.Now.AddYears(-calculatedAge))
                    calculatedAge--;
                age = calculatedAge.ToString();
            }
            return age;
        }

        private void SetModel(PersonViewModel personViewModel, ref Person person)
        {
            person.PersonId = personViewModel.PersonId;
            person.FirstName = personViewModel.FirstName;
            person.Surname = personViewModel.Surname;
            person.DOB = personViewModel.DOB;
            List<int> personAttributeIds = new List<int>();

            foreach (var personAttributeViewModel in personViewModel.PersonAttributes)
            {
                var personAttribute = person.PersonAttributes.FirstOrDefault(pa => pa.AttributeId == personAttributeViewModel.Attribute.AttributeId);
                if (personAttribute == null)
                {
                    personAttribute = new Core.Model.PersonAttribute();
                    personAttribute.AttributeId = personAttributeViewModel.Attribute.AttributeId;
                    personAttribute.AttributeDescription = personAttributeViewModel.AttributeDescription;
                    person.PersonAttributes.Add(personAttribute);
                }
                else
                {
                    personAttribute.AttributeDescription = personAttributeViewModel.AttributeDescription;
                }
                personAttributeIds.Add(personAttribute.PersonAttributeId);
            }
           
            //for(int count = person.PersonAttributes.Count()-1; count>= 0; count--)
            //{
            //    if (!personAttributeIds.Contains(person.PersonAttributes[count].PersonAttributeId))
            //        person.PersonAttributes.Remove(person.PersonAttributes[count]);
            //}
        }


        //private void SetModel(PersonViewModel personViewModel, ref Person person)
        //{
        //    person.PersonId = personViewModel.PersonId;
        //    person.FirstName = personViewModel.FirstName;
        //    person.Surname = personViewModel.Surname;
        //    person.DOB = personViewModel.DOB;

        //    foreach (var personAttributeViewModel in personViewModel.PersonAttributes)
        //    {
        //        var personAttribute = person.PersonAttributes.FirstOrDefault(pa => pa.AttributeId == personAttributeViewModel.Attribute.AttributeId);
        //        if (personAttribute == null)
        //        {
        //            personAttribute = new Core.Model.PersonAttribute();
        //            personAttribute.AttributeId = personAttributeViewModel.Attribute.AttributeId;
        //            personAttribute.AttributeDescription = personAttributeViewModel.AttributeDescription;
        //            person.PersonAttributes.Add(personAttribute);
        //        }
        //        else { 
        //            personAttribute.AttributeDescription = personAttributeViewModel.AttributeDescription;
        //        }
        //    }

        //}
        
        private void PopulateAttributes()
        {
            var attributes = attributeService.GetAttributes()
                                             .Select(a => new AttributeViewModel()
                                                    {
                                                        AttributeId= a.AttributeId,
                                                        AttributeName = a.AttributeName
                                                    }).ToList();
            
            ViewData["attributeList"] = attributes;
        }
    }
}