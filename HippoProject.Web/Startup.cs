﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HippoProject.Web.Startup))]
namespace HippoProject.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
