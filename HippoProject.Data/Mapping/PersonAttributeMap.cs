﻿using HippoProject.Core;
using HippoProject.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HippoProject.Data.Mapping
{

    public partial class PersonAttributeMap: HippoEntityTypeConfiguration<PersonAttribute>
    {
        public PersonAttributeMap()
        {
            this.ToTable("PersonAttribute");
            this.HasKey(pa => pa.PersonAttributeId);
            this.Property(pa => pa.AttributeDescription);
        }
    }
}
