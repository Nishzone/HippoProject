﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HippoProject.Data.Mapping
{

    public partial class AttributeMap: HippoEntityTypeConfiguration<Core.Model.Attribute>
    {
        public AttributeMap()
        {
            this.ToTable("Attribute");
            this.HasKey(a => a.AttributeId);
            this.Property(a => a.AttributeName).IsRequired().HasMaxLength(50);
            this.HasMany(a => a.PersonAttributes).WithRequired(a => a.Attribute).WillCascadeOnDelete(false);
        }
    }
}
