﻿using HippoProject.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HippoProject.Data.Mapping
{

    public partial class PersonMap: HippoEntityTypeConfiguration<Person>
    {
        public PersonMap()
        {
            this.ToTable("Person");
            this.HasKey(p => p.PersonId);
            this.Property(p => p.FirstName).IsRequired().HasMaxLength(50);
            this.Property(p => p.Surname).IsRequired().HasMaxLength(50);
            this.Property(p => p.DOB);
            this.HasMany(p => p.PersonAttributes).WithRequired(p => p.Person).WillCascadeOnDelete(true);

        }
    }
}
