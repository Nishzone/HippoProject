namespace HippoProject.Data
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using Core;
    using System.Reflection;
    using System.Data.Entity.ModelConfiguration;
    using Mapping;
    using System.Data.Entity.Infrastructure;

    public partial class HippoDbContext : DbContext, IDbContext
    {
        public HippoDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString) { }


        public new DbEntityEntry Entry<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            return base.Entry<TEntity>(entity);
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        //public virtual DbSet<Core.Attribute> Attributes { get; set; }
        //public virtual DbSet<Person> People { get; set; }
        //public virtual DbSet<PersonAttribute> PersonAttributes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Core.Attribute>()
            //    .Property(e => e.AttributeName)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Core.Attribute>()
            //    .HasMany(e => e.PersonAttributes)
            //    .WithRequired(e => e.Attribute)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Person>()
            //    .Property(e => e.FirstName)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Person>()
            //    .Property(e => e.Surname)
            //    .IsUnicode(false);

            //modelBuilder.Entity<Person>()
            //    .HasMany(e => e.PersonAttributes)
            //    .WithRequired(e => e.Person)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<PersonAttribute>()
            //    .Property(e => e.AttributeDescription)
            //    .IsUnicode(false);

            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
           .Where(type => !String.IsNullOrEmpty(type.Namespace))
           .Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
               type.BaseType.GetGenericTypeDefinition() == typeof(HippoEntityTypeConfiguration<>));
            foreach (var type in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(type);
                modelBuilder.Configurations.Add(configurationInstance);
            }

            base.OnModelCreating(modelBuilder);
        }
    }
}
