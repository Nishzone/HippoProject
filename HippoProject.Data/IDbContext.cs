﻿using System.Collections.Generic;
using System.Data.Entity;
using HippoProject.Core;
using System.Data.Entity.Infrastructure;

namespace HippoProject.Data
{
    public interface IDbContext
    {
        /// <summary>
        /// Get DbSet
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <returns>DbSet</returns>
        IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity;

        /// <summary>
        /// Save changes
        /// </summary>
        /// <returns></returns>
        int SaveChanges();


        DbEntityEntry Entry<TEntity>(TEntity entity) where TEntity : BaseEntity;
    }
}
