using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HippoProject.Core.Model
{
    public partial class Attribute: BaseEntity
    {
        private ICollection<PersonAttribute> _personAttributes;

        public int AttributeId { get; set; }

        public string AttributeName { get; set; }


        public virtual ICollection<PersonAttribute> PersonAttributes
        {
            get { return _personAttributes ?? (_personAttributes = new List<PersonAttribute>()); }
            protected set { _personAttributes = value; }
        }
    }
}
