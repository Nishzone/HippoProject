using System;
using System.Collections.Generic;

namespace HippoProject.Core.Model
{
    public partial class PersonAttribute: BaseEntity
    {

        public int PersonAttributeId { get; set; }

     
        public int PersonId { get; set; }

        public int AttributeId { get; set; }

        public string AttributeDescription { get; set; }

        public virtual Attribute Attribute { get; set; }

        public virtual Person Person { get; set; }

    }
}
