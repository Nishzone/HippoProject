﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HippoProject.Core.Model
{ 
    public partial class Person: BaseEntity
    {
        private IList<PersonAttribute> _personAttributes;

        public int PersonId { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public DateTime? DOB { get; set; }

        public virtual IList<PersonAttribute> PersonAttributes
        {
            get { return _personAttributes ?? (_personAttributes = new List<PersonAttribute>()); }
            set { _personAttributes = value; }
        }
     }
}
