USE [master]
GO

IF EXISTS(SELECT name FROM sys.databases WHERE name = 'Hippo')
     DROP DATABASE [Hippo]
GO
CREATE DATABASE [Hippo]
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Hippo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Hippo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Hippo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Hippo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Hippo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Hippo] SET ARITHABORT OFF 
GO
ALTER DATABASE [Hippo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Hippo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Hippo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Hippo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Hippo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Hippo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Hippo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Hippo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Hippo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Hippo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Hippo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Hippo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Hippo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Hippo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Hippo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Hippo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Hippo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Hippo] SET RECOVERY FULL 
GO
ALTER DATABASE [Hippo] SET  MULTI_USER 
GO
ALTER DATABASE [Hippo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Hippo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Hippo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Hippo] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Hippo', N'ON'
GO
USE [Hippo]
GO

/****** Object:  Table [dbo].[Attribute]    Script Date: 20/01/2017 17:19:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Attribute](
	[AttributeId] [int] IDENTITY(1,1) NOT NULL,
	[AttributeName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Attribute] PRIMARY KEY CLUSTERED 
(
	[AttributeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Person]    Script Date: 20/01/2017 17:19:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[PersonId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[Surname] [varchar](50) NOT NULL,
	[DOB] [datetime] NULL,
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[PersonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersonAttribute]    Script Date: 20/01/2017 17:19:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonAttribute](
	[PersonAttributeId] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[AttributeId] [int] NOT NULL,
	[AttributeDescription] [varchar](50) NULL,
 CONSTRAINT [PK_PersonAttribute] PRIMARY KEY CLUSTERED 
(
	[PersonAttributeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Attribute] ON 

GO
INSERT [dbo].[Attribute] ([AttributeId], [AttributeName]) VALUES (1, N'Height')
GO
INSERT [dbo].[Attribute] ([AttributeId], [AttributeName]) VALUES (2, N'Hair Colour')
GO
INSERT [dbo].[Attribute] ([AttributeId], [AttributeName]) VALUES (3, N'Weight')
GO
INSERT [dbo].[Attribute] ([AttributeId], [AttributeName]) VALUES (9, N'Eye Colour')
GO
SET IDENTITY_INSERT [dbo].[Attribute] OFF
GO
SET IDENTITY_INSERT [dbo].[Person] ON 

GO
INSERT [dbo].[Person] ([PersonId], [FirstName], [Surname], [DOB]) VALUES (1, N'Garth', N'Drummond', CAST(N'1980-01-01T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Person] ([PersonId], [FirstName], [Surname], [DOB]) VALUES (2, N'Matthew', N'Hood', CAST(N'1980-02-01T00:00:00.000' AS DateTime))
GO
INSERT [dbo].[Person] ([PersonId], [FirstName], [Surname], [DOB]) VALUES (6, N'Nisha', N'Appanah', CAST(N'1985-07-01T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Person] OFF
GO
SET IDENTITY_INSERT [dbo].[PersonAttribute] ON 

GO
INSERT [dbo].[PersonAttribute] ([PersonAttributeId], [PersonId], [AttributeId], [AttributeDescription]) VALUES (1, 1, 1, N'170 cm')
GO
INSERT [dbo].[PersonAttribute] ([PersonAttributeId], [PersonId], [AttributeId], [AttributeDescription]) VALUES (5, 2, 1, N'175 cm')
GO
INSERT [dbo].[PersonAttribute] ([PersonAttributeId], [PersonId], [AttributeId], [AttributeDescription]) VALUES (1003, 6, 1, N'154')
GO
INSERT [dbo].[PersonAttribute] ([PersonAttributeId], [PersonId], [AttributeId], [AttributeDescription]) VALUES (1005, 2, 2, N'blond')
GO
INSERT [dbo].[PersonAttribute] ([PersonAttributeId], [PersonId], [AttributeId], [AttributeDescription]) VALUES (1010, 1, 2, N'Brown')
GO
INSERT [dbo].[PersonAttribute] ([PersonAttributeId], [PersonId], [AttributeId], [AttributeDescription]) VALUES (1012, 1, 9, N'Brown')
GO
INSERT [dbo].[PersonAttribute] ([PersonAttributeId], [PersonId], [AttributeId], [AttributeDescription]) VALUES (1017, 6, 9, N'Black')
GO
SET IDENTITY_INSERT [dbo].[PersonAttribute] OFF
GO
ALTER TABLE [dbo].[PersonAttribute]  WITH CHECK ADD  CONSTRAINT [FK_PersonAttribute_Attribute] FOREIGN KEY([AttributeId])
REFERENCES [dbo].[Attribute] ([AttributeId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PersonAttribute] CHECK CONSTRAINT [FK_PersonAttribute_Attribute]
GO
ALTER TABLE [dbo].[PersonAttribute]  WITH CHECK ADD  CONSTRAINT [FK_PersonAttribute_Person] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([PersonId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PersonAttribute] CHECK CONSTRAINT [FK_PersonAttribute_Person]
GO
USE [master]
GO
ALTER DATABASE [Hippo] SET  READ_WRITE 
GO
