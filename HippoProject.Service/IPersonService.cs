﻿using HippoProject.Core;
using HippoProject.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HippoProject.Service
{
    public interface IPersonService
    {
        IPagedList<Person> GetPeople(string firstName = "", string surname = "",
            int pageIndex = 0, int pageSize = int.MaxValue, bool sortByFirstName = false, bool sortBySurname = false);

        Person GetPersonById(int personId);

        void InsertPerson(Person person);

        void UpdatePerson(Person person, IEnumerable<int> activePersonAttributes);

        void DeletePerson(Person person);
    }
}
