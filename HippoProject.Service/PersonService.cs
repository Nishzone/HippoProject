﻿
using HippoProject.Core;
using HippoProject.Core.Model;
using HippoProject.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HippoProject.Service
{
    public class PersonService : IPersonService
    {

        #region Fields

        private readonly IRepository<Person> personRepository;
        private readonly IRepository<PersonAttribute> personAttributeRepository;
        #endregion


        #region Constructor
        public PersonService(IRepository<Person> personRepository, IRepository<PersonAttribute> personAttributeRepository)
        {
            this.personRepository = personRepository;
            this.personAttributeRepository = personAttributeRepository;
        }
        #endregion


        #region Methods

        public IPagedList<Person> GetPeople(string firstName = "", string surname = "",
            int pageIndex = 0, int pageSize = int.MaxValue, bool sortByFirstName = false, bool sortBySurname = false)
        {
            var query = personRepository.Table;

            if (!String.IsNullOrWhiteSpace(firstName))
                query = query.Where(p => p.FirstName.Contains(firstName));

            if (!String.IsNullOrWhiteSpace(surname))
                query = query.Where(p => p.Surname.Contains(surname));

            if (sortByFirstName)
                query = query.OrderBy(p => p.FirstName);

            if (sortBySurname)
                query = query.OrderBy(p => p.Surname);


            var people = query.ToList();

            return new PagedList<Person>(people, pageIndex, pageSize);
        }

        public Person GetPersonById(int personId)
        {
            if (personId == 0)
                return null;

            return personRepository.GetById(personId);
        }

        public void InsertPerson(Person person)
        {
            if (person == null)
                throw new ArgumentNullException("person");

            personRepository.Insert(person);
        }

        public void UpdatePerson(Person person, IEnumerable<int> activePersonAttributes)
        {
            if (person == null)
                throw new ArgumentNullException("person");

            for (int count = person.PersonAttributes.Count()-1; count >= 0; count--)
            {
                if (!activePersonAttributes.Contains(person.PersonAttributes[count].PersonAttributeId))
                    personAttributeRepository.SetStateToDeleted(person.PersonAttributes[count]);
            }
            
            personRepository.Update(person);
        }

        public void DeletePerson(Person person)
        {
            if (person == null)
                throw new ArgumentNullException("person");

            personRepository.Delete(person);
        }


        #endregion
    }
}
