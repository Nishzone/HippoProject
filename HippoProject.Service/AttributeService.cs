﻿using HippoProject.Core;
using HippoProject.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HippoProject.Core.Model;

namespace HippoProject.Service
{
    public class AttributeService: IAttributeService
    {
        #region Fields

        private readonly IRepository<Core.Model.Attribute> attributeRepository;

        #endregion


        #region Constructor

        public AttributeService(IRepository<Core.Model.Attribute> attributeRepository)
        {
            this.attributeRepository = attributeRepository;
        }

        #endregion


        #region Methods

        public IPagedList<Core.Model.Attribute> GetAttributes(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = attributeRepository.Table;
            var attributes = query.ToList();
            return new PagedList<Core.Model.Attribute>(attributes, pageIndex, pageSize);
        }

        public Core.Model.Attribute GetAttributeById(int attributeId)
        {
            if (attributeId == 0)
                return null;

            return attributeRepository.GetById(attributeId);
        }


        public void InsertAttribute(Core.Model.Attribute attribute)
        {
            if (attribute == null)
                throw new ArgumentNullException("attribute");

            attributeRepository.Insert(attribute);
        }

        public void UpdateAttribute(Core.Model.Attribute attribute)
        {
            if (attribute == null)
                throw new ArgumentNullException("attribute");

            attributeRepository.Update(attribute);
        }

        public void DeleteAttribute(Core.Model.Attribute attribute)
        {
            if (attribute == null)
                throw new ArgumentNullException("attribute");

            attributeRepository.Delete(attribute);
        }


        #endregion
    }
}
