﻿using HippoProject.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HippoProject.Service
{
    public interface IAttributeService
    {
        IPagedList<Core.Model.Attribute> GetAttributes(int pageIndex = 0, int pageSize = int.MaxValue);

        void InsertAttribute(Core.Model.Attribute attribute);

        void UpdateAttribute(Core.Model.Attribute attribute);

        void DeleteAttribute(Core.Model.Attribute attribute);

        Core.Model.Attribute GetAttributeById(int attributeId);
     }
}
